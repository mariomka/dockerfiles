FROM php:8.3-alpine

WORKDIR /var/www

RUN rm -rf /var/www/html

RUN apk add --update --no-cache \
    # Install dependencies
    autoconf \
    g++ \
    icu-dev \
    make \
    postgresql-dev \
    freetype \
    gmp-dev \
    libpng \
    jpeg-dev \
    libjpeg-turbo \
    libpng-dev \
    libjpeg-turbo-dev \
    libwebp \
    libwebp-dev \
    libzip-dev \
    zlib-dev \
    libxpm-dev \
    freetype-dev \
    # Install image optimizers (svgo is excluded because Node.js isn't available)
    jpegoptim \
    optipng \
    pngquant \
    gifsicle \
    libwebp \
    # Install extensions
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
	&& docker-php-ext-configure intl \
	&& docker-php-ext-install -j "$(nproc)" intl pdo_pgsql gd zip bcmath gmp \
    # Clean up
    && rm -rf /tmp/pear \
    && apk del \
    autoconf \
    g++ \
    make \
    libpng-dev \
    libjpeg-turbo-dev \
    libwebp-dev \
    zlib-dev \
    libxpm-dev \
    freetype-dev

# Install composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Configure PHP
RUN mv "$PHP_INI_DIR/php.ini-development" "$PHP_INI_DIR/php.ini" \
    && echo 'memory_limit = 256M' >> "$PHP_INI_DIR/conf.d/docker-php-memlimit.ini"
