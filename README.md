# Dockerfiles

## How to build and push

Login to Docker account

```shell
docker login
```

### mariomka/ci-deploy
```shell
docker build -t mariomka/ci-deploy:[NEW_TAG] ci/deploy
docker push mariomka/ci-deploy:[NEW_TAG]
```

### mariomka/ci-php8.1
```shell
docker build -t mariomka/ci-php8.1:[NEW_TAG] ci/php8.1
docker push mariomka/ci-php8.1:[NEW_TAG]
```

### mariomka/ci-php8.3
```shell
docker build -t mariomka/ci-php8.3:[NEW_TAG] ci/php8.3
docker push mariomka/ci-php8.3:[NEW_TAG]
```
